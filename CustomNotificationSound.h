@interface BBContent : NSObject
@property (copy, nonatomic) NSString *message;
@property (copy, nonatomic) NSString *subtitle;
@property (copy, nonatomic) NSString *title;
@end

@interface BBBulletin : NSObject
@property (copy, nonatomic) NSString *section;
@property (retain, nonatomic) BBContent *content;
@end

@interface BBSound : NSObject
+ (instancetype)alertSoundWithSystemSoundPath:(id)arg1;
@end

@interface PSSpecifier : NSObject
@property (retain, nonatomic) NSString *identifier;
@end

@interface PSTableCell : UITableViewCell
@property (retain, nonatomic) PSSpecifier *specifier;
@end

@interface BBSectionInfo : NSObject
@property (copy, nonatomic) NSString *sectionID;
@property (copy, nonatomic) NSString *displayName;
@end

@interface BulletinBoardAppDetailController : UIViewController 
- (BBSectionInfo *)effectiveSectionInfo;
- (NSNumber *)_valueOfNotificationType:(unsigned int)arg1;
- (void)_setValue:(NSNumber *)arg1 notificationType:(unsigned int)arg2;
@end
